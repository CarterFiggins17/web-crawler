from bs4 import BeautifulSoup
import requests
import sys
from urllib.parse import urlparse

visited = []

def toHTML(url):
    try:
        response = requests.get(url)
        return response.text

    except Exception as e:
        # In case an error happens:
        print(f"Failed to get {url} because {e}")

def start():
    if len(sys.argv) < 2:
        url = 'http://example.com/'
        print(f"Error: no URL supplied default \'{url}\'")
        depth = 3
    elif len(sys.argv) < 3: #TODO: need a error when url is not an Absolute URL.
        url = sys.argv[1]
        parsed = urlparse(url)
        if parsed.scheme:
           depth = 3
        else:
            print("No Scheme!")  #TODO: what does it realy need to say (look at notes)
            sys.exit(1)
        if parsed.netloc:
            depth = 3
        else:
            print("No location!")
            sys.exit(1)

    else:
        url = sys.argv[1]
        depth = sys.argv[2]

    print(f"Crawling form {url} to maximum depth of {depth} links")
    return depth, url,


def findLinks(html):
    try:
        soup = BeautifulSoup(html, 'html.parser')
        links = soup.find_all('a')
        return links

    except Exception as e:
        print(f"An exception occurred: {e}")
    print()




def crawl(url, depth, maxdepth, visited):

    #Base case
    if int(depth) >= int(maxdepth):
        depth = depth - 1
        return

    #makes sure we don't vist the same url
    if url in visited:
        return

    # change the url to html
    html = toHTML(url)
    if(html == None):
        return


    #finds the links in the html
    links = findLinks(html)


    numOfSpaces = ""
    if depth > 0:
        numOfSpaces = "   " * depth
    print(numOfSpaces + url)

    #add url to visited list
    visited.append(url)

    atParsed = urlparse(url)
    atSchee = atParsed.scheme
    atLocation = atParsed.netloc

    try:
        if links:
            # going thur all links in Url
            for a in links:
                #getting the new url
                newUrl = a.get('href')

                newParsed = urlparse(newUrl)

                if newParsed.scheme:     #TODO: what if it was just https?
                    pass
                else:
                    if newParsed.netloc:
                        newUrl = f"{atSchee}://{newUrl}"
                    else:
                        newUrl = f"{atSchee}://{atLocation}{newUrl}"

                # if not hitMax and not hitVisited:
                crawl(newUrl, depth + 1, maxdepth, visited)


    except Exception as e:
            print(f"An exceptions occurred: {e}")




depth, url = start()
start = 0
crawl(url, start, int(depth) +1, visited)
